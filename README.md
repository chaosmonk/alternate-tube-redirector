## Alternate Tube Redirector

![Alternate Tube Redirector](assets/icon-96.png)

Redirects YouTube video links to other alternative sites like invidious, hooktube and others.

more info at [mozilla addon page](https://addons.mozilla.org/firefox/addon/alternate-tube-redirector/)