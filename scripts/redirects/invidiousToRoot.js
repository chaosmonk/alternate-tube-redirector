registerRequestsListener(
    'youtube_alt',
    async (request) => {
        let newUrl = new URL(request.url);
        newUrl.hostname = 'invidio.us';
        return {
            redirectUrl: newUrl.toString()
        };
    },
    [
        '*://www.invidio.us/*'
    ],
    ['main_frame']
);