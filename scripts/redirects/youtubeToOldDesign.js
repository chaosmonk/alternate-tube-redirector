registerRequestsListener(
    'youtube_without_polymer',
    async (request) => {
        let url = new URL(request.url);
        if(!url.searchParams.has('disable_polymer')) {
            if(request.type === 'main_frame') {
                let params = new URLSearchParams(url.search);
                params.set('disable_polymer', true);
                url.search = params.toString();
                const newUrl = url.toString();
                return {
                    redirectUrl: newUrl
                };
            } else {
                await browser.tabs.reload(request.tabId);
            }
        }
    },
    [
        '*://www.youtube.com/',
        '*://www.youtube.com/feed/trending*',
        '*://www.youtube.com/channel/*',
        '*://www.youtube.com/user/*',
        '*://www.youtube.com/playlist*',
        '*://www.youtube.com/results*'
    ],
    ['main_frame', 'xmlhttprequest']
);