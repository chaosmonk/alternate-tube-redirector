registerRequestsListener(
    'invidious_channel_on_youtube',
    async (request) => {
        const newUrl = replace_with_youtube(request.url);
        return {
            redirectUrl: newUrl
        };
    },
    [
        '*://invidio.us/channel/*'
    ],
    ['main_frame']
);