function from_alt_video_page(fromUrl) {
    if (fromUrl) {
        const url = new URL(fromUrl);
        if (url.hostname === 'hooktube.com') return true;
        if (url.hostname === 'invidio.us') return true;
    }
    return false;
}

registerRequestsListener(
    'watch_on_alt',
    async (request) => {
        let url = new URL(request.url);
        if ((url.pathname === '/watch') && !from_alt_video_page(request.originUrl)) {
            if (request.type === 'main_frame') {
                let newUrl = await replace_with_alt(request.url);
                if (url.searchParams.has('list') && !url.searchParams.has('index')) {
                    url.pathname = '/playlist';
                    const list = url.searchParams.get('list');
                    url.search = '';
                    url.searchParams.set('list', list);
                    newUrl = url.toString();
                }
                return {
                    redirectUrl: newUrl
                };
            } else {
                await browser.tabs.reload(request.tabId);
            }
        }
    },
    [
        '*://www.youtube.com/watch*',
    ],
    ['main_frame', 'xmlhttprequest']
);