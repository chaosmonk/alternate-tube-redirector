registerRequestsListener(
    'invidious_search_on_youtube',
    async (request) => {
        const newUrl = invidious_search_to_youtube(request.url);
        return {
            redirectUrl: newUrl
        };
    },
    [
        '*://invidio.us/search*'
    ],
    ['main_frame']
);