registerRequestsListener(
    'watch_on_alt',
    async (request) => {
        const url = new URL(request.url);
        const hash = url.pathname.split('/')[2];
        const newUrl = `https://www.youtube.com/watch?v=${hash}`;
        return {
            redirectUrl: newUrl
        };
    },
    [
        '*://www.youtube.com/embed/*'
    ],
    ['main_frame']
);