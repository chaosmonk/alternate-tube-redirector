function channelToIgnore(path) {
    const channelId = path[1];
    const channelBlacklist = [
        'UC-9-kyTW8ZkZNDHQJ6FgpwQ', // Music
        'UCEgdi0XIXXZ-qJOFPf4JSKw', // Sports
        'UCOpNcN46UbXVtpKMrmU4Abg', // Gaming
        'UClgRkhTL3_hImCAmdLfDE4g', // Movies
        'UCY9Jh3SK0N1SAVJi-U--Rwg', // TV series
        'UCYfdidRxbB8Qhf0Nx7ioOYw', // News
        'UC4R8DWoMoI7CAwX8_LjQHig', // Live
        'UCvScgo6mAvbMEjszK4sSj6g', // Spotlight
        'UCzuqhhs6NWbgTzMuM09WKDQ', // 360 videos
    ];
    return (channelBlacklist.indexOf(channelId) > -1) ? true : false;
}

registerRequestsListener(
    'channel_videos_tab',
    async (request) => {
        let url = new URL(request.url);
        let path = url.pathname.split('/').filter(Boolean);
        if(path.length === 2 && !channelToIgnore(path)) {
            if(request.type === 'main_frame') {
                path.push('videos');
                url.pathname = '/' + path.join('/');
                const newUrl = url.toString();
                return {
                    redirectUrl: newUrl
                };
            } else {
                await browser.tabs.reload(request.tabId);
            }
        }
    },
    [
        '*://*.youtube.com/channel/*',
        '*://*.youtube.com/user/*',
    ],
    ['main_frame', 'xmlhttprequest']
);