async function updateButtonInfo() {
    const option = 'watch_on_alt';
    const defaultOption = {[option]: true};
    const savedOptions = await browser.storage.local.get(defaultOption);
    if (savedOptions[option]) {
        await browser.browserAction.setTitle({title: 'Alternate Tube Redirector'});
        await browser.browserAction.setIcon({path: '../assets/icon-48.png'});
    } else {
        await browser.browserAction.setTitle({title: '[OFF] Alternate Tube Redirector'});
        await browser.browserAction.setIcon({path: '../assets/icon-48-gray.png'});
    }
}
