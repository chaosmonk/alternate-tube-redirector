async function currentAlternateHost() {
    const option = 'youtube_alt';
    const defaultOption = {[option]: defaultConfigOptions[option]};
    const savedOption = await browser.storage.local.get(defaultOption);
    const altHosts = {
        'hooktube': 'hooktube.com',
        'invidious': 'invidio.us',
    };
    return altHosts[savedOption[option]];
}

async function replace_with_alt(url) {
    let curl = new URL(url);
    curl.hostname = await currentAlternateHost();
    return curl.toString();
}

function replace_with_youtube(url) {
    let curl = new URL(url);
    curl.hostname = 'www.youtube.com';
    return curl.toString();
}

function invidious_search_to_youtube(url) {
    let curl = new URL(url);
    curl.hostname = 'www.youtube.com';
    curl.pathname = '/results';
    curl.searchParams.append('search_query', curl.searchParams.get('q'));
    curl.searchParams.delete('q');
    return curl.toString();
}
