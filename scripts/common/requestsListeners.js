async function listenerOnRequests(option, reqHandler, reqUrls, reqFrame, rev) {
    const defaultOption= {[option]: defaultConfigOptions[option]};
    const savedOptions = await browser.storage.local.get(defaultOption);

    if(savedOptions[option]) {
        if (rev)
            browser.webRequest.onBeforeRequest.removeListener(reqHandler);
        else
            browser.webRequest.onBeforeRequest.addListener(
                reqHandler,
                { urls: reqUrls, types: reqFrame },
                ['blocking']
            );
    } else {
        if (rev)
            browser.webRequest.onBeforeRequest.addListener(
                reqHandler,
                { urls: reqUrls, types: reqFrame },
                ['blocking']
            );
        else
            browser.webRequest.onBeforeRequest.removeListener(reqHandler);
    }
}

function watchOnStorageChange(option, reqHandler, reqUrls, reqFrame, rev) {
    browser.storage.onChanged.addListener((changes, area) => {
        if (area === 'local' && option in changes) {
            listenerOnRequests(option, reqHandler, reqUrls, reqFrame, rev);
        }
    });
}

function registerRequestsListener(option, reqHandler, reqUrls, reqFrame, rev=false) {
    watchOnStorageChange(option, reqHandler, reqUrls, reqFrame, rev);
    listenerOnRequests(option, reqHandler, reqUrls, reqFrame, rev);
}