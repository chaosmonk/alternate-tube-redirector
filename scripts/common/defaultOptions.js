const defaultConfigOptions = {
    'watch_on_alt': true,
    'channel_videos_tab': true,
    'youtube_without_polymer': false,

    'youtube_alt': 'invidious',

    'hooktube_search_on_youtube': false,
    'hooktube_channel_on_youtube': false,

    'invidious_autoplay_video': false,
    'invidious_search_on_youtube': false,
    'invidious_channel_on_youtube': false,
};
